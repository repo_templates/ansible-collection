#!/bin/bash
set -e

if [[ ! -d "./.git" ]]; then
    git init -b "main"
fi
git config --global --add safe.directory `pwd`
if [[ ! $(git config --get user.name) ]]; then
    read -p "Enter git name: " GIT_NAME
    git config user.name "${GIT_NAME}"
fi
if [[ ! $(git config --get user.email) ]]; then
    read -p "Enter git email: " GIT_EMAIL
    git config user.email "${GIT_EMAIL}"
fi

poetry install --with dev
poetry run pre-commit install

echo "eval \`ssh-agent -s\`" >> ~/.bashrc
