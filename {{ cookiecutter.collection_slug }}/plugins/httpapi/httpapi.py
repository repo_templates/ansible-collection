#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Copyright: {{ cookiecutter.author }}
# <short license line>

DOCUMENTATION = r"""
---
module:
    The name of the module (must be the same as the filename).
short_description:
    Enough detail to explain the module's purpose without the context of the directory structure in which it lives.  Should NOT end in a period.
description:
    Detailed description (two or more sentences) which doesn't mention the module's name.  Can provide these via a list of lines vs. one long paragraph.
version_added:
	The version of the collection when this module was added.
author:
    The name of the author in the form `First Last (@GithubID)`.  Use a multi-line list if there are multiple authors
deprecated:
    Modules that will be removed in future releases.
options:
    The specification of arguments for the modules
    option-name:
        Declarative operation (focus on final state rather than CRUD)
        description:
            Detailed explanation of what the option does.  First entry is about the option itself, subsequent entries detail its use, dependencies, or format of possible values.  If it is only sometimes required, describe the conditions.  Mutually exclusive options must be documented as the final sentence on each of the options.
		required:
            Only needed if `True`
		default:
            If `required` is false/missing, it may be specified otherwise `null` if missing.  Ensure it matches what is in the code.
		choices:
            List of option values, should be absent if empty.
		type:
            The data type the option accepts.  If it is a collection type, `elements` needs to be specified.
		elements:
            The data type for the elements of a collection.
		aliases:
            List of optional name aliases.
		version_added:
            Only need this if option was added after initial release (i.e. version is greater than top-level `version-added` field.).
		suboptions:
            If this option takes a dict or list of dicts you can define the structure here.
requirements:
    List of requirements (if applicable).
seealso:
    A list of references to other modules, documentation or internet resources.  Can be of the form:
    - `module: <>`;  `module: <>`, `description: <>`
    - `plugin: `, `plugin_type: <>`;`plugin: `, `plugin_type: <>`, `description: <>`
    - `ref: <>`, `description: <>`
    - `name <>`, `description: <>`, `link: <>`
attributes:
    A dict of attribute names to dicts describing that attribute.
    description:
        A string or list of strings explaining what this attribute does.
    details:
        A string or list of strings describing how support might not work as expected by the user.
    support:
        One of `full`, `none`, `partial`, or `N/A`.  This is required.  Indicates whether this attribute is supported by this module or plugin.
    membership:
        A string or list of strings listing the action groups this module is a part of (only used for attribute `action_group`)
    platforms:
        A string or list of strings listing the platforms the module or action supports (only used for attribute `platforms`).
    version_added:
        Only needed if the attribute was extended after the module/plugin was created.
notes:
    Details of any important information that doesn't fit in one of the above sections.  Information on `check_mode` or `diff` should not be listed here, but instead mentioned in `attributes`.
"""

#try:
#    from lib import function
#    HAS_LIB = True
#except ImportError:
#    HAS_LIB = False

import json

from ansible.module_utils.basic import missing_required_lib

from ansible.module_utils.basic import to_text
from ansible.module_utils.connection import ConnectionError
from ansible.plugins.httpapi import HttpApiBase

BASE_HEADERS = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

class HttpApi(HttpApiBase):
    def __init__(self, *args, **kwargs):
        super(HttpApi, self).__init__(*args, **kwargs)

    def login(self, username, password):
        login_path = ""
        credentials = {"username": username, "password": password}
        response = self.send_request(path=login_path, data=json.dumps(credentials), method="POST")
        # If the cookie is not in "Set-Cookie" or needs to be in "Cookie"
        #try:
        #    self.connection._auth = {"X-API-TOKEN": response["token"]}
        #except KeyError:
        #    raise AnsibleAuthenticationFailure(message="Failed to acquire login token.")

    def logout(self):
        logout_path = ""
        self.send_request(None, path=logout_path)
        self.connection._auth = None

    def send_request(self, data, path, method="GET", **message_kwargs):
        response, response_data = self.connection.send(path, data, method=method, headers=BASE_HEADERS, **message_kwargs)
        try:
            response_data = json.loads(to_text(response_data.getvalue()))
        except ValueError:
            raise ConnectionError(f"Response was not valid JSON: {to_text(response_data.getvalue())}")
        return response_data
