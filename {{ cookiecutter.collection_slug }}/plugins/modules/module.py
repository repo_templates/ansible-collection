#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Copyright: {{ cookiecutter.author }}
# <short license line>

DOCUMENTATION = r"""
---
module:
    The name of the module (must be the same as the filename).
short_description:
    Enough detail to explain the module's purpose without the context of the directory structure in which it lives.  Should NOT end in a period.
description:
    Detailed description (two or more sentences) which doesn't mention the module's name.  Can provide these via a list of lines vs. one long paragraph.
version_added:
	The version of the collection when this module was added.
author:
    The name of the author in the form `First Last (@GithubID)`.  Use a multi-line list if there are multiple authors
deprecated:
    Modules that will be removed in future releases.
options:
    The specification of arguments for the modules
    option-name:
        Declarative operation (focus on final state rather than CRUD)
        description:
            Detailed explanation of what the option does.  First entry is about the option itself, subsequent entries detail its use, dependencies, or format of possible values.  If it is only sometimes required, describe the conditions.  Mutually exclusive options must be documented as the final sentence on each of the options.
		required:
            Only needed if `True`
		default:
            If `required` is false/missing, it may be specified otherwise `null` if missing.  Ensure it matches what is in the code.
		choices:
            List of option values, should be absent if empty.
		type:
            The data type the option accepts.  If it is a collection type, `elements` needs to be specified.
		elements:
            The data type for the elements of a collection.
		aliases:
            List of optional name aliases.
		version_added:
            Only need this if option was added after initial release (i.e. version is greater than top-level `version-added` field.).
		suboptions:
            If this option takes a dict or list of dicts you can define the structure here.
requirements:
    List of requirements (if applicable).
seealso:
    A list of references to other modules, documentation or internet resources.  Can be of the form:
    - `module: <>`;  `module: <>`, `description: <>`
    - `plugin: `, `plugin_type: <>`;`plugin: `, `plugin_type: <>`, `description: <>`
    - `ref: <>`, `description: <>`
    - `name <>`, `description: <>`, `link: <>`
attributes:
    A dict of attribute names to dicts describing that attribute.
    description:
        A string or list of strings explaining what this attribute does.
    details:
        A string or list of strings describing how support might not work as expected by the user.
    support:
        One of `full`, `none`, `partial`, or `N/A`.  This is required.  Indicates whether this attribute is supported by this module or plugin.
    membership:
        A string or list of strings listing the action groups this module is a part of (only used for attribute `action_group`)
    platforms:
        A string or list of strings listing the platforms the module or action supports (only used for attribute `platforms`).
    version_added:
        Only needed if the attribute was extended after the module/plugin was created.
notes:
    Details of any important information that doesn't fit in one of the above sections.  Information on `check_mode` or `diff` should not be listed here, but instead mentioned in `attributes`.
"""

EXAMPLES = r"""
-  Show users how your module works with real-world examples in multi-line plain-text YAML.  Best examples are ready to copy and paste into a playbook.
- If your module returns facts that are often needed, an example of how to use them can be helpful.
"""

RETURN = r"""
If your module doesn't return anything apart from standard, this section should read: `RETURN = r''' # '''``
return name:
    Name of the returned field
	description:
        Detailed description of what this value represents.
	returned:
        When the value is returned (`always`, `changed`, `success`, etc)
	type:
        Data type
	elements:
        if the `type` is a collection, this specifies the data type of the elements
	sample:
        One or more examples.
	version_added:
        Only needed if it was extended after initial release.
	contains:
        Optional field for collection `type`s to hold the same elements above describing sub-fields.
"""

#try:
#    from lib import function
#    HAS_LIB = True
#except ImportError:
#    HAS_LIB = False

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.basic import missing_required_lib
#from ansible.module_utils.module_util_lib import module_util_function

def main():
    #if not HAS_LIB:
    #    module.fail_json(msg=missing_required_lib("lib"))

    """
    Argument:
        type:
            Can be any of: str, list, dict, bool, int, float, path, raw,
            jsonarg, json, bytes, bits.
        elements:
            If an argument is a container type then elements should be
            defined.  Can be any of the same types as "type".
        default:
            The default value when an argument is not set; default is None.
        fallback:
            Tuple where the first argument is a callable to perform a lookup
            based on the second argument.  The second argument is the arguments
            to the callable.
        choices:
            List of choices that the argument will accept.
        required:
            Whether the argument is required or not; default is False.
        no_log:
            Whether or not an argument should be masked in logs and output.
        aliases:
            List of alternative names for the argument.
        options:
            Defines sub-Argument Spec
        apply_defaults:
            Works with "options" and allows the "default" of sub-options to be
            applied even when the top-level argument is not supplied.
        removed_in_version:
            Which version of the collection an argument will be deprecated in.
            Mutually exclusive with "removed_at_date" and must be used with
            "removed_from_collection".
        removed_at_date:
            The date after which a deprecated argument will be removed.
            Mutually exclusive with "removed_in_version" and must be used with
            "removed_from_collection".
        removed_from_collection:
            The collection that deprecated the argument.  Must be used with
            "removed_in_version" or "removed_at_date".
        deprecated_aliases:
            List or tuple of dicts specifying the deprecated aliases of the
            argument.  Dicts inside tuple contain:
                - name, version or date, and collection_name
    """
    argument_spec = dict(
    )

    """
    argument_spec:
        The specifications for the arguments of the module.
    supports_check_mode:
        Whether or not the module supports Ansible checks.
    If "options is specified:
    mutually_exclusive:
        A list of tuples containing sub-options that are mutually exclusive.
    required_together:
        A list of tuples containing sub-options that need to be used together.
    required_one_of:
        A list of tuples containing sub-options, one if which needs to be used.
    required_if:
        A list of tuples where the first element is the option name; the second is the target value; the third is a tuple of sub-options; and the fourth
        specifies whether all of the supplied sub-options are necessary, or
        only one.
    required_by:
        A dict of sub-options to sequences of sub-options that must be used if
        the key sub-option is used.
    """
    module = AnsibleModule(
        argument_spec=argument_spec,
    )

    result = dict(changed=False)

    # Determine the current state of the system by connecting and querying.

    # Determine the desired state of the system by reading module arguments.
    #   module.params.get("argument")

    # Determnie the changes that need to be made to make the current state
    # match the desired state.

    if module.check_mode:
        # Update "result" dictionary with details about current state, desired
        # state, and changes to be made.
        pass

    # Send the commands to the system and make the current state equal the
    # desired state.
    #   Update result dictionary to show "changed" is "True" and provide
    #   results.

    if result:
        module.exit_json(**result)

if __name__ == "__main__":
    main()
