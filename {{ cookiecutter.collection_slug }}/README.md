# {{ cookiecutter.namespace }} {{ cookiecutter.collection }}

{{ cookiecutter.description }}

## Installation

### From Git
```bash
ansible-galaxy collection install git+https://gitlab.com/{{ cookiecutter.collection_slug }}.git
```
or create a `requirements.yml` file with the following:
```yaml
collections:
- name: https://gitlab.com/{{ cookiecutter.collection_slug }}.git
  type: git
  version: main
```

### From Tarball
Create a `requirements.yml` file with the following:
```yaml
collections:
- name: /path/to/tarball
  type: file
```

### From Source
After cloning the repository you can install the whole namespace with:
```bash
ansible-galaxy collection install /path/to/{{ cookiecutter.collection_slug }} -p ./collections
```
or create a `requirements.yml` file with the following:
```yaml
collections:
- name: /path/to/{{ cookiecutter.collection_slug }}
  type: subdirs
```

## Usage

Once installed, you can reference collections by its FQCN:
```yaml
- name: Reference a collection content using its FQCN
  hosts: all
  tasks:
    - name: Call a module using FQCN
      {{ cookiecutter.namespace_slug }}.{{ cookiecutter.collection_slug }}.module:
        option1: value
```
or by using the `collections` keyword:
```yaml
- name: Reference a collection content the collections keyword
  hosts: all
  collections:
    - {{ cookiecutter.namespace_slug }}.{{ cookiecutter.collection_slug }}
  tasks:
    - name: Call a module using FQCN
      module:
        option1: value
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update create/tests as appropriate.

This project includes a development container environment for Visual Studio Code to make development more stable and easier to start.

See [development](docs/development.md) for more information on commit message format, branching, versioning, testing, documentation and release steps.

If you want to contribute and need a path, check out [todo](docs/TODO.md) or the issues for this project.
