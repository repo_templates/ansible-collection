#!/bin/bash

echo -e "[  ] Initializing environment..."

REGISTRY="registry.gitlab.com"
echo -e "\tREGISTRY: ${REGISTRY}"
IMAGE_NAME="${REGISTRY}/repo_templates/ansible-collection"
echo -e "\tIMAGE_NAME: ${IMAGE_NAME}"
GIT_REPO="${GIT_REPO:-$(git ls-remote --get-url)}"
echo -e "\tGIT_REPO: ${GIT_REPO}"
GIT_BRANCH="${GIT_BRANCH:-$(git rev-parse --abbrev-ref HEAD)}"
echo -e "\tGIT_BRANCH: ${GIT_BRANCH}"
GIT_COMMIT="${GIT_COMMIT:-$(git rev-parse HEAD)}"
echo -e "\tGIT_COMMIT: ${GIT_COMMIT}"
GIT_COMMIT_SHORT="$(echo "${GIT_COMMIT}" | cut -c 1-8)"
echo -e "\tGIT_COMMIT_SHORT: ${GIT_COMMIT_SHORT}"
GIT_USER="${GIT_USER:-$(git config --get user.name)}"
echo -e "\tGIT_USER: ${GIT_USER}"
GIT_EMAIL="${GIT_EMAIL:-$(git config --get user.email)}"
echo -e "\tGIT_EMAIL: ${GIT_EMAIL}"
BUILD_TIME="$(date -u +"%Y-%0m-%0dT%0H:%M:%SZ")"
echo -e "\tBUILD_TIME: ${BUILD_TIME}"
VERSIONS=("$(git describe --tags | tr -d '[g]')")
if [[ "${GIT_BRANCH}" == "main" ]]; then
    MAJOR_VER="$(echo ${VERSIONS[0]} | cut -d . -f 1)"
    MINOR_VER="$(echo ${VERSIONS[0]} | cut -d . -f 2)"
    VERSIONS+=("${MAJOR_VER}.${MINOR_VER}" "${MAJOR_VER}" "latest")
else
    VERSIONS+=("${GIT_BRANCH}")
fi
echo -e "\tVERSIONS: ${VERSIONS[@]}"

echo -e "[OK] Initialized environment."


echo -e "[  ] Building..."

docker buildx build --platform linux/amd64,linux/arm64/v8 \
    --tag "${IMAGE_NAME}:${VERSIONS[0]}" \
    --build-arg "GIT_REPO=${GIT_REPO}" \
    --build-arg "GIT_BRANCH=${GIT_BRANCH}" \
    --build-arg "GIT_COMMIT=${GIT_COMMIT}" \
    --build-arg "GIT_USER=${GIT_USER}" \
    --build-arg "GIT_EMAIL=${GIT_EMAIL}" \
    --build-arg "BUILD_TIME=${BUILD_TIME}" \
    --build-arg "VERSION=${VERSIONS[0]}" \
    .

for VERSION in "${VERSIONS[@]:1}"; do
    docker tag "${IMAGE_NAME}:${VERSIONS[0]}" "${IMAGE_NAME}:${VERSION}"
done

echo -e "[OK] Built."

if [[ "${1}" == "--publish" ]]; then
    echo -e "[  ] Publishing..."

    docker login "${REGISTRY}"
    docker push -a "${IMAGE_NAME}"

    echo -e "[OK] Published."
fi
