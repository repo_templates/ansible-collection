FROM almalinux:latest

ARG VERSION
ARG GIT_REPO
ARG GIT_BRANCH
ARG GIT_COMMIT
ARG GIT_USER
ARG GIT_EMAIL
ARG BUILD_TIME

ENV VERSION="${VERSION}"

LABEL version="${VERSION}"
LABEL git_repository="${GIT_REPO}"
LABEL git_branch="${GIT_BRANCH}"
LABEL git_commit="${GIT_COMMIT}"
LABEL git_user="${GIT_USER}"
LABEL git_email="${GIT_EMAIL}"
LABEL build_time="${BUILD_TIME}"

RUN dnf update -y && \
    dnf install -y epel-release && \
    dnf install -y sudo \
        tar \
        gnupg \
        openssh \
        git \
        gcc \
        python3.11 \
        python3.11-devel \
        python3.11-pip \
        ansible && \
    dnf clean all

RUN pip3.11 install --no-cache-dir poetry

RUN groupadd --gid 1000 vscode && \
    useradd --uid 1000 --gid 1000 -m vscode && \
    echo vscode ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/vscode && \
    chmod 0440 /etc/sudoers.d/vscode

USER vscode

CMD ["/bin/bash"]
