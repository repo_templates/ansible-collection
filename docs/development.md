# Development

## Semantic Commit Messages
[qoomon/conventional_commit_messages](https://gist.github.com/qoomon/5dfcdf8eec66a051ecd85625518cfd13)

Commit messages should be of the form:
```
[<type>](<optional scope>): <description>

<optional body>

<optional footer(s)>
```

### Types

* `feat` - new feature for the user; not a new feature for build
* `fix` - bug fix for the user; not a fix to a build
* `refactor` - refactoring production code; does not change behavior
* `perf` - refactors that are specific to improving performance
* `test` - adding missing tests, refactoring tests; do not affect behavior
* `docs` - changes to the documentation
* `style` - formatting, missing semi colons, etc; do not affect behavior
* `build` - affect build components
* `ops` - affect deployments, backups, recoveries, etc
* `chore` - miscellaneous tasks; do not affect behavior

### Scopes

Provides additional information about what component of the code, not issue identifiers.  Keep them small.

### Breaking Changes / Major Changes

When making a change that will break backwards compatibility, add the `!`
identifier after the scope:

`[type](scope)!: <description> ...`

In addition, add a `BREAKING CHANGES:` section in the footer that explains what will break.

### Description

Use the imperative, present tense: "change" instead of "changed" or "changes".
Don't capitalize the first letter.  Don't add a period at the end.

### Body

Should include the motivation for the change and constrast with previous behavior.  Use the imperative, present tense: "change" instead of "changed" or "changes".

### Footer

Should contain any information about breaking changes and/or reference issue identifiers.

### Example

```
[feat](core)!: remove the use() command

This command was previously marked as deprecated and a new feature has been
deployed to take its place.  Instead of running use() to perform the operation,
it has been moved to be a part of the main function.

Issue: !2319
BREAKING CHANGES: Core no longer supports the operation via function call.
```

## Branching

* `master`
        * infinite lifetime; production ready state
* `dev`
        * infinite lifetime; latest development changes for next release
* hotfixes
    * must branch off of `master`
    * must merge back into (`dev`|release) and `master`
    * name: `hotfix-<fix>`
    * fix a critical bug; then bump versions and whatnot like a release
    * merged into `master` and tagged for easier reference
* release branches
    * must branch off of `dev`
    * must merge back into `dev` and `master`
    * name: `release-<version>`
    * branch off when `dev` has enough changes for a release
    * final touches are done before being made a release
        * version bumping
        * cleaning
        * bugfixes
    * merged into `master` and tagged for easier reference
* feature branches
    * used to develop new features for upcoming or distant releases
    * must branch off of `dev`
    * must mege back into `dev`
    * name: `feat-<feature>`

```
master  hotfixes  releases  dev  features
  |                          |
  0 0.1.0                    O
  |\                         |
  | *----------------------. |
  |  \                      \|
  |   *--.                   O------.
  |       \                  |\      \
  |        O                 O *--.   O
  |       / \                |     \  |
  | .----*   *-------------. O      O O
  |/                        \|\     | |
  O 0.1.1                    O *--. O O
  |\                         |     X  |
  | *----.                   O .--* \ O
  |       \                  |/      \|
  |        O                 O        O
  |        |                /|       /
  |        O           .---* | .----*
  |        |          /      |/
  |        O         O       O
  |       / \        |       |\
  | .----*   *-----.-O-----. O *-.
  |/                \|      \|    \
  O 0.1.2            O       O     O
  |                  |       |     |
  |                  O       O     O
  |                 / \      |     |
  | .--------------*   *---. O     O
  |/                        \|     |
  O 1.0.0                    O     O
  |                         /|    /
  |                    .---* | .-*
  |                   O      |/
  |                  / \     O
  | .---------------*   *--. |
  |/                        \|
  O 1.1.0                    O
  |                          |
```

## Merging Branches

When merging branches, make sure to squash, fast-forward (no merge commit), and
delete the source branch on merge.
Not squashing is only permitted if there is a valid reason.

## Versioning

* Major incremented when you make incompatible API changes
* Minor incremented when you add functionality that is backwards compatible
* Patch incremented when you make backward compatible bug fixes
* additional labels for pre-release and build metadata can be used
    * alpha, beta, release candidate, etc

# Release Steps

1. create a new release branch from `dev`
2. update all the version numbers
3. check documentation, testing, commenting, style, etc
4. review changes once more
5. merge with `master` and `dev`
6. add tag at that commit for the version
7. package new release for that version
