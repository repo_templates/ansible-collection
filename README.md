# Ansible Collection Repository Template

This repository holds a cookiecutter template for Ansible Collections.

## Installation

To use these templates you need to have cookiecutter installed either through
`brew`, `pip` or `pipx`.

## Usage

To create a repository from this templates, use:

```bash
cookiecutter https://gitlab.com/repo_templates/ansible-collection.git
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update create/tests as appropriate.

This project includes a development container environment for Visual Studio Code to make development more stable and easier to start.

See [development](docs/development.md) for more information on commit message format, branching, versioning, testing, documentation and release steps.

If you want to contribute and need a path, check out [todo](docs/TODO.md) or the issues for this project.

### Updating the Development Container

When updating the development container on Windows you will need to ensure you
have WSL installed to run the `./build.sh` script.

In addition, no matter the OS you need to have `docker buildx` installed so you can deploy for multiple architectures.
